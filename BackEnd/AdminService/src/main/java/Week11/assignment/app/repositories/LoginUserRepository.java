package Week11.assignment.app.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import Week11.assignment.app.entities.LoginUser;


@Repository
public interface LoginUserRepository extends CrudRepository<LoginUser, String>{
	public List<LoginUser> findByType(String type);
}
