package week11.assignment.app.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ReadLater {
	
	@Id
	@Column(columnDefinition = "int(10)")
	private int bookId;
	
	@Column(columnDefinition = "varchar(25)")
	private String bookName;
	
	@Column(columnDefinition = "varchar(25)")
	private String bookGenre;
	
	@Column(columnDefinition = "varchar(30)")
	private String email;
	
	public ReadLater() {
		
	}
	
	public ReadLater(int bookId, String bookName, String bookGenre, String email) {
		super();
		this.bookId = bookId;
		this.bookName = bookName;
		this.bookGenre = bookGenre;
		this.email = email;
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getBookGenre() {
		return bookGenre;
	}

	public void setBookGenre(String bookGenre) {
		this.bookGenre = bookGenre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "ReadLater [bookId=" + bookId + ", bookName=" + bookName + ", bookGenre=" + bookGenre + ", email="
				+ email + "]";
	}
	
}
