package week11.assignment.app.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import week11.assignment.app.entities.Book;
import week11.assignment.app.repositories.BookRepository;

@Service
public class BookService {
	
	@Autowired
	private BookRepository bookRepository;
	
//	public boolean bookAlreadyExists(Book book) {
//		if(this.bookRepository.existsById(book.getBookId())) {
//			return true;
//		}
//		this.bookRepository.save(book);
//		return true;
//	}
	
	public List<Book> getAllBooks() {
		
		List<Book> listOfBooks = new ArrayList<>();
		this.bookRepository.findAll().forEach((books -> listOfBooks.add(books)));
		return listOfBooks;
	}
	
	public Book getBook(int id) {
		if(this.bookRepository.existsById(id)) {
			Book bk = this.bookRepository.findById(id).get();
			System.out.println(bk);
			return bk;
		}
		return null;
		
	}

	public boolean addBookDetails(Book book) {
		if(this.bookRepository.existsById(book.getBookId())) {
			return false;
		}
		this.bookRepository.save(book);
		return true;
	}

	public boolean deleteBook(int id) {
		if(this.bookRepository.existsById(id)) {
			this.bookRepository.deleteById(id);
			return true;
		}
		return false;
	}

	public boolean updateBook(Book book) {
		if(this.bookRepository.existsById(book.getBookId())) {
			this.bookRepository.save(book);
			return true;
		}
		return false;
	}
}
