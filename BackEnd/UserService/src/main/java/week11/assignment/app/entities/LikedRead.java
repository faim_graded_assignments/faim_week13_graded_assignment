package week11.assignment.app.entities;

public class LikedRead {
	
	private int bookId;
	private String bookName;
	private String bookGenre;
	private String email;
	
	public LikedRead() {
		
	}

	public LikedRead(int bookId, String bookName, String bookGenre, String email) {
		super();
		this.bookId = bookId;
		this.bookName = bookName;
		this.bookGenre = bookGenre;
		this.email = email;
	}

	public int getBookid() {
		return bookId;
	}

	public void setBookid(int bookiId) {
		this.bookId = bookId;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getBookGenre() {
		return bookGenre;
	}

	public void setBookGenre(String bookGenre) {
		this.bookGenre = bookGenre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "LikedRead [bookid=" + bookId + ", bookName=" + bookName + ", bookGenre=" + bookGenre + ", email="
				+ email + "]";
	}
	
}