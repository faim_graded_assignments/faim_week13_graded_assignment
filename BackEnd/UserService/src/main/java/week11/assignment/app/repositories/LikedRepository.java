package week11.assignment.app.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import week11.assignment.app.entities.Liked;


@Repository
public interface LikedRepository extends CrudRepository<Liked, Integer>{

}
