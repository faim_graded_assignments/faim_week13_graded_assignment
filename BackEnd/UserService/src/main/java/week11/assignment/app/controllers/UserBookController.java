package week11.assignment.app.controllers;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import week11.assignment.app.entities.Book;
import week11.assignment.app.entities.LoginUser;


@RestController
@CrossOrigin
public class UserBookController {

	@Autowired
	private RestTemplate template;


	//***********************************************Book Operations****************************************
	@GetMapping("/books")
	public String getAllBooks(HttpSession session) {

		String url = "http://ADMIN-SERVICE/book";

		ResponseEntity<Book[]> listOfBooks = this.template.getForEntity(url, Book[].class);

		List<Book> listOfAllBooks = Arrays.asList(listOfBooks.getBody());

		if(listOfAllBooks.isEmpty()) {
			session.setAttribute("mssg", "List Of Books is Empty");
			return "redirect:/message";
		}else {
			session.setAttribute("list", listOfAllBooks);
			System.out.println(listOfAllBooks);
			return "book";
		}
	}

}
