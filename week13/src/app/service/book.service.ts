import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Book } from '../models/Book';
import { Message } from '../models/Message';

@Injectable({
  providedIn: 'root'
})
export class BookService {
  
  

  httpBook = "http://localhost:8082/";
  httpBookLiked = "http://localhost:8081/liked/";
  constructor(private http:HttpClient) {

  }

  getBooks():Observable<Book[]>{

    return this.http.get<Book[]>(
      this.httpBook+"book"
    )

  }

  deleteBook(bookId:number):Observable<Message>{
    return this.http.get<Message>(
      this.httpBook+"delete/"+bookId
    )
  }

  addBook(book: Book):Observable<Message> {

    return this.http.post<Message>(
      this.httpBook+"addbook",book
    )
  }

  likeBook(liked:number,email:string):Observable<Message> {
    return this.http.get<Message>(
      this.httpBookLiked+"addliked/"+liked+"/"+email
    )
  }
}
