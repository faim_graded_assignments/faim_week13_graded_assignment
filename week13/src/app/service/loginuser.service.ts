import { Injectable } from '@angular/core';
import { LoginUser } from '../models/LoginUser';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Message } from '../models/Message';
import { Book } from '../models/Book';

@Injectable({
  providedIn: 'root'
})
export class LoginuserService {
  
  
  
  urlUser = "http://localhost:8081/";
  urlAdmin = "http://localhost:8082/";
  constructor(private http:HttpClient,private router:Router) { }

  // when user try to login
  registerAdmin(us: LoginUser):Observable<Message> {
    
    return this.http.post<Message>(
      this.urlAdmin+'registered',us);
  }

  registerUser(us: LoginUser):Observable<Message> {
    
    return this.http.post<Message>(
      this.urlAdmin+'adduser',us);
  }

  loginUser(email:string){
    return this.http.get<Message>(
      this.urlAdmin+'login/'+email);
  }

  loginAdmin(email:string):Observable<Message>{
    return this.http.get<Message>(
      this.urlAdmin+'login/'+email);
  }

  deleteUser(email:string):Observable<Message> {
    return this.http.get<Message>(
      this.urlAdmin+'deleteuser/'+email);
  }

  getUsers():Observable<LoginUser[]> {
    return this.http.get<LoginUser[]>(
      this.urlAdmin+"users"
    )
  }

}
