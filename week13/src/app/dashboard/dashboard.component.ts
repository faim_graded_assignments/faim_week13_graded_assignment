import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Book } from '../models/Book';
import { LikedRead } from '../models/likedread';
import { Message } from '../models/Message';
import { BookService } from '../service/book.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  listOfBooks: Book[] = [];
  selectedValue: string = '';
  email: any = '';
  bookId: number = 0;
  id:any;

  key:any; 
  isError: boolean = false;
  errMessage: string = '';
  wantToDelete: boolean = false;
  constructor(private serv: BookService, private router: Router) {
    this.getBooks();
  }

  ngOnInit(): void {
  }

  getBooks() {

    this.serv.getBooks().subscribe((data: any) => {

      console.log(this.listOfBooks);

      console.log(data);
      this.listOfBooks = data;
    })

  }

  isLoggedIn() {
    this.email = sessionStorage.getItem('email');
    if (this.email !== null && this.email.endsWith("hcl.com")) {
      return true;
    } else {
      return false;
    }
  }

  deleteBook(data: any) {
    let that = this;
    this.bookId = data;
    this.wantToDelete = confirm("Are You Sure You Want to Delete the Book");
    if (this.wantToDelete) {
      this.serv.deleteBook(this.bookId).subscribe({
        next(data: Message) {
          console.log(data.message);
          alert("Book deleted");
          that.router.navigate(['/dashboard']);
        },
        error(data) {
          alert("Book not Found");
          console.log('failed');
          console.log(data.error);
          that.isError = true;
          that.errMessage = data.error.description;
        }
      })
    } else {
      this.ngOnInit();
    }
  }

  likeBook(book: Book) {
    this.key = sessionStorage.getItem('email');
    let that = this;
    let like = new LikedRead(book.bookId,book.bookName,book.bookGenre,this.key);
    console.log(book.bookId+" "+book.bookName+" "+book.bookGenre);
    console.log(this.key);
    this.serv.likeBook(book.bookId,this.key).subscribe({
      next(data: Message) {
        console.log(data.message);
        alert("Book Added To Liked");
        that.router.navigate(['/liked']);
      },
      error(data) {
        alert("Book Already is in liked");
        console.log('failed');
        console.log(data.error);
        that.isError = true;
        that.errMessage = data.error.description;
      }
    })
  }

  logOut(){
    this.id = sessionStorage.getItem('email');
    if(this.id !== null){
      sessionStorage.removeItem('email');
    }
  }

}
