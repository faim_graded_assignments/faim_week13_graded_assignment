export class LikedRead{

    public bookId:number;
    public bookName:string;
    public bookGenre:string;
    public email:string;

    constructor(bookId:number,bookName:string,bookGenre:string,email:string){
        this.bookId = bookId;
        this.bookName = bookName;
        this.bookGenre = bookGenre;
        this.email = email;
    }
}