export class Book{

    public bookId:number;
    public bookName:string;
    public bookGenre:string;

    constructor(bookId: number,bookName: string,bookGenre: string){
        this.bookId = bookId;
        this.bookName = bookName;
        this.bookGenre = bookGenre;
    }

    
    public get getBookId() : number {
        return this.bookId;
    }
    
    
    public set setBookId(bookId : number) {
        this.bookId = bookId
    }

    public get getBookName() : string {
        return this.bookName;
    }
    
    
    public set setBookName(bookName : string) {
        this.bookName = bookName;
    }
    
    public get getBookGenre() : string {
        return this.bookGenre;
    }
    
    
    public set setBookGenre(bookGenre : string) {
        this.bookGenre = bookGenre;
    }

}