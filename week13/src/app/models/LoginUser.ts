export class LoginUser{

    email:string;
    name:string;
    password:string;

    constructor(email:string,name:string,password:string){
        this.email = email;
        this.name = name;
        this.password = password;
    }

    
    public get getEmail() : string {
        return this.email;
    }

    
    public set setEmail(email : string) {
        this.email = email;
    }

    public get getName() : string {
        return this.name;
    }

    
    public set setName(name : string) {
        this.name = name;
    }

    public get getPassword() : string {
        return this.password;
    }

    
    public set setPassowrd(password : string) {
        this.password = password;
    }
    
    
}