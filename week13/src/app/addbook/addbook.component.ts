import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Book } from '../models/Book';
import { Message } from '../models/Message';
import { BookService } from '../service/book.service';
import { LoginuserService } from '../service/loginuser.service';

@Component({
  selector: 'app-addbook',
  templateUrl: './addbook.component.html',
  styleUrls: ['./addbook.component.css']
})
export class AddbookComponent implements OnInit {


  isError: boolean = false;
  errMessage: string = '';
  email: string = '';
  id:any;

  constructor(private serv: BookService, private router: Router) {

  }

  ngOnInit(): void {
  }

  addBook(data:any){

    let that = this;
    this.email = data.email;
    let book = new Book(data.book, data.name, data.genre);
    this.serv.addBook(book).subscribe({
      next(data: Message) {
        console.log(data.message);
        alert("Book Added");
        that.router.navigate(['/dashboard']);
      },
      error(data) {
        alert("Registration Already Exist");
        console.log('failed');
        console.log(data.error);
        that.isError = true;
        that.errMessage = data.error.description;
      }
    })

  }

  logOut(){
    this.id = sessionStorage.getItem('email');
    if(this.id !== null){
      sessionStorage.removeItem('email');
    }
  }

}
