import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Book } from '../models/Book';
import { LoginUser } from '../models/LoginUser';
import { Message } from '../models/Message';
import { LoginuserService } from '../service/loginuser.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  isError: boolean = false;
  errMessage: string = '';
  email: string = '';
  
  constructor(private serv:LoginuserService,private router:Router) {

  }


  loginUser(data: any) {
    let that = this;
    this.email = data.email;
    let us = new LoginUser(data.email,data.name,data.password);
    if(this.email.endsWith("hcl.com")){

      this.serv.loginAdmin(us.getEmail).subscribe({
        next(data: Message) {
          console.log(data.message);
          sessionStorage.setItem('email', that.email);
          alert("You are Logged in")
          that.router.navigate(['/dashboard']);
        },
        error(data) {
          alert("Login Failed");
          console.log('failed');
          console.log(data.error);
          that.isError = true;
          that.errMessage = data.error.description;
        }
      })

    }else{
      this.serv.loginUser(this.email).subscribe({
        next(data: Message) {
          console.log(data.message);
          sessionStorage.setItem('email', that.email);
          that.router.navigate(['/dashboard']);
        },
        error(data) {
          console.log('failed');
          console.log(data.error);
          that.isError = true;
          that.errMessage = data.error.description;
        }
      })
    }
    
  }
}
