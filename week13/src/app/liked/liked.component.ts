import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Book } from '../models/Book';
import { LoginUser } from '../models/LoginUser';
import { Message } from '../models/Message';
import { BookService } from '../service/book.service';
import { LoginuserService } from '../service/loginuser.service';

@Component({
  selector: 'app-liked',
  templateUrl: './liked.component.html',
  styleUrls: ['./liked.component.css']
})
export class LikedComponent implements OnInit {

  id:any;
  listOfBooks:Book[]=[];
  email:any;
  constructor(private serv:LoginuserService,private router:Router,private service:BookService) {
    this.getBooks();
  }

  ngOnInit(): void {
    console.log("on init")
  }

  logOut(){
    this.id = sessionStorage.getItem('email');
    if(this.id !== null){
      sessionStorage.removeItem('email');
    }
  }

  getBooks() {

    this.service.getBooks().subscribe((data: any) => {

      console.log(this.listOfBooks);

      console.log(data);
      this.listOfBooks = data;
    })

  }

  isLoggedIn() {
    this.email = sessionStorage.getItem('email');
    if (this.email !== null && this.email.endsWith("hcl.com")) {
      return true;
    } else {
      return false;
    }
  }

}
