import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginUser } from '../models/LoginUser';
import { Message } from '../models/Message';
import { LoginuserService } from '../service/loginuser.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  listOfUsers:LoginUser[] = [];
  id:any;
  wantToDelete:boolean = false;
  isError: boolean = false;
  errMessage: string = '';
  email: string = '';
  constructor(private us: LoginuserService, private router: Router) {
    this.getUsers();
  }

  ngOnInit(): void {

  }

  getUsers(){
    this.us.getUsers().subscribe((data: any) => {

      console.log(this.listOfUsers);

      console.log(data);
      this.listOfUsers = data;
    })
  }

  logOut(){
    this.id = sessionStorage.getItem('email');
    if(this.id !== null){
      sessionStorage.removeItem('email');
    }
  }

  deleteUser(data:any){
    let that = this;
    this.email = data.email;
    this.wantToDelete = confirm("Are You Sure You want to Delete");
    if(this.wantToDelete){
      this.us.deleteUser(data).subscribe({
        next(data: Message) {
          console.log(data.message);
          alert("User deleted");
          that.router.navigate(['/dashboard']);
        },
        error(data) {
          alert("User not Found");
          console.log('failed');
          console.log(data.error);
          that.isError = true;
          that.errMessage = data.error.description;
        }
      })
    }else{
      this.ngOnInit();
    }
  }

}
