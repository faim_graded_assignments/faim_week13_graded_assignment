import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginUser } from '../models/LoginUser';
import { Message } from '../models/Message';
import { LoginuserService } from '../service/loginuser.service';

@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.css']
})
export class AdduserComponent implements OnInit {

  isError: boolean = false;
  errMessage: string = '';
  email: string = '';
  id:any;

  constructor(private serv: LoginuserService, private router: Router) { }

  ngOnInit(): void {
  }

  registerUser(data: any) {
    let that = this;
    this.email = data.email;
    let us = new LoginUser(data.email, data.name, data.password);
    this.serv.registerUser(us).subscribe({
      next(data: Message) {
        console.log(data.message);
        alert("Registration Successfull");
        that.router.navigate(['/dashboard']);
      },
      error(data) {
        alert("Registration Failed");
        console.log('failed');
        console.log(data.error);
        that.isError = true;
        that.errMessage = data.error.description;
      }
    })
  }
  logOut(){
    this.id = sessionStorage.getItem('email');
    if(this.id !== null){
      sessionStorage.removeItem('email');
    }
  }

}
