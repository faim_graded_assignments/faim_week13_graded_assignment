import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BookComponent } from './book/book.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LikedComponent } from './liked/liked.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import {AdduserComponent} from './adduser/adduser.component';
import { DeleteUserComponent } from './delete-user/delete-user.component';
import { AddbookComponent } from './addbook/addbook.component';
import { UsersComponent } from './users/users.component';

const routes: Routes = [
  
  // {path:'',redirectTo:'login',pathMatch:'full'},
  {path:'',component:LoginComponent},
  {path:'books',component:BookComponent},
  {path:'liked',component:LikedComponent},
  {path:'register',component:RegisterComponent},
  {path:'dashboard',component:DashboardComponent},
  {path:'adduser',component:AdduserComponent},
  {path:'deleteuser',component:DeleteUserComponent},
  {path:'addbook',component:AddbookComponent},
  {path:'users',component:UsersComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
