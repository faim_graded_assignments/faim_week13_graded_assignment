import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { Book } from '../models/Book';
import { BookService } from '../service/book.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent{

  listOfBooks:any[] = [];
  
  constructor(private service:BookService,private router:Router){
    this.getBooks();
  }

  getBooks(){
    this.service.getBooks().subscribe((data: any) => {

      console.log(this.listOfBooks);
      console.log(data);

      this.listOfBooks = data;
      
    })
  }
}
