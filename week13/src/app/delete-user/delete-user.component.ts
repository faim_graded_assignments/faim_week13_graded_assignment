import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginUser } from '../models/LoginUser';
import { Message } from '../models/Message';
import { LoginuserService } from '../service/loginuser.service';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.css']
})
export class DeleteUserComponent implements OnInit {


  isError: boolean = false;
  errMessage: string = '';
  email: string = '';
  id:any;
  constructor(private serv: LoginuserService, private router: Router) { }

  ngOnInit(): void {
  }

  deleteUser(data:any){
    let that = this;
    this.email = data.email;
    this.serv.deleteUser(this.email).subscribe({
      next(data: Message) {
        console.log(data.message);
        alert("User deleted");
        that.router.navigate(['/dashboard']);
      },
      error(data) {
        alert("User not Found");
        console.log('failed');
        console.log(data.error);
        that.isError = true;
        that.errMessage = data.error.description;
      }
    })
  }

  logOut(){
    this.id = sessionStorage.getItem('email');
    if(this.id !== null){
      sessionStorage.removeItem('email');
    }
  }

}
