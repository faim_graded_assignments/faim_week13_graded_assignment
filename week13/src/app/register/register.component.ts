import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginUser } from '../models/LoginUser';
import { Message } from '../models/Message';
import { LoginuserService } from '../service/loginuser.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {


  isError: boolean = false;
  errMessage: string = '';
  email: string = '';
  constructor(private serv:LoginuserService,private router: Router) { }

  ngOnInit(): void {
  }

  registerUser(data: any) {
    let that = this;
    this.email = data.email;
    let us = new LoginUser(data.email,data.name,data.password);
    if(this.email.endsWith("hcl.com")){

      this.serv.registerAdmin(us).subscribe({
        next(data: Message) {
          console.log(data.message);
          alert("Registration Successfull");
          that.router.navigate(['']);
        },
        error(data) {
          alert("Registration Failed");
          console.log('failed');
          console.log(data.error);
          that.isError = true;
          that.errMessage = data.error.description;
        }
      })

    }else{
      
      this.serv.registerUser(us).subscribe({
        next(data: Message) {
          console.log(data.message);
          alert("Registration Successfull");
          that.router.navigate(['']);
        },
        error(data) {
          alert("Registration Failed");
          console.log('failed');
          console.log(data.error);
          that.isError = true;
          that.errMessage = data.error.description;
        }
      })

    }
  }
}
